# disk-imaging-resources

Like https://gitlab.com/EG-tech/emulation-resources but resources for creating and manipulating disk images

# Contents
- [Software](#software)
- [Floppy Disk Controllers](#floppy-disk-controllers)
- [Optical Media Ripping Hardware](#optical-media-ripping-hardware)
- [Reading, Guides, Resources](#reading-guides-resources)

---

## Software

### Image Acquisition

- [Aaru Data Preservation Suite](https://www.aaru.app) (wide variety of legacy media formats, including floppy, optical, disk, tape)
- [Brasero](https://wiki.gnome.org/Apps/Brasero/) (optical media; Linux)
- [cdrdao](http://cdrdao.sourceforge.net/) (Audio/Data CDs)
- [cdrkit](https://en.wikipedia.org/wiki/Cdrkit) (optical media)
    - [omimgr](https://github.com/KBNLresearch/omimgr) (optical media; simple GUI/wrapper for cdrkit and ddrescue; Linux)
- [dBpoweramp](https://dbpoweramp.com/) (Audio CDs; Windows + MacOS)
- [dd](https://en.wikipedia.org/wiki/Dd_(Unix)) (block storage/media)
- [ddrescue](https://www.gnu.org/software/ddrescue/) (block storage/media)
    - [diskimgr](https://github.com/KBNLresearch/diskimgr) (hard disk and floppy formats; simple GUI/wrapper for dd and ddrescue; Linux)
- [DiscImageCreator](https://github.com/saramibreak/DiscImageCreator) (various optical and disk media; Windows/Linux)
    - [MPF](https://github.com/SabreTools/MPF) (GUI for DiscImageCreator; Windows-only)
- [dvdistaster](https://dvdisaster.jcea.es/) (optical media; Linux)
- [Exact Audio Copy](http://exactaudiocopy.de/) (Audio CDs; Windows)
- FTK Imager  
Access Data are cops, won't link :upside_down_face: but I guess people use it
- [Guymager](https://guymager.sourceforge.io/) (block storage/media; Linux)
- [ImgBurn](http://imgburn.com/) (optical media; Windows)
- [IsoBuster](https://www.isobuster.com/) (optical media, Windows)
- [myrescue](https://github.com/daald/myrescue) (hard disks, "similar in purpose to ddrescue" but designed specifically for damaged disks; Linux)
- [tapeimgr](https://github.com/KBNLresearch/tapeimgr) (tape formats; simple GUI/wrapper for dd and mt; Linux)
- [tools-for-unusual-tape-formats](https://github.com/larsbrinkhoff/tools-for-unusual-tape-formats) (rare/obscure tape formats; source code for Unix utilities, may be helpful for super rare or challenging stuff)
- [RawWrite](http://www.chrysocome.net/rawwrite) (3.5" floppies, Windows)
- [WinImage](https://winimage.com/) (various formats, Windows)

### Image Manipulation
- [Apple Commander](https://applecommander.github.io) (Apple II images - AppleDOS, ProDOS, etc)
- [AppleSauce client](https://applesaucefdc.com/software/) (AppleSauce host software, can be used without hardware but macOS-only)
- [CiderPress II](https://ciderpress2.com/) (various types of Apple image, as well as working directly with some old Apple software/files)
- [Disk Jockey](https://diskjockey.onegeekarmy.eu/) (Classic Mac and PC image creation/manipulation/analysis)
- [dtc](https://kryoflux.com/?page=download) (Kryoflux host software, can be used without hardware)
- [fluxengine](http://cowlark.com/fluxengine/) (FluxEngine host software, can be used without hardware)
- [LIBDSK](http://www.seasip.info/Unix/LibDsk/index.html)
- [PCE tools](http://www.hampa.ch/pce/download.html) (PC emulator but contains tools e.g. "pfi" for manipulating and converting images)
- [SAMdisk](https://simonowen.com/samdisk/)
- [QEMU](https://www.qemu.org/) (PC emulator but contains tools e.g. "qemu-img" for manipulating and converting images)

## Floppy Disk Controllers
- [AppleSauce](https://applesaucefdc.com/) (started for Apple 3.5 and 5.25"; can do IBM PC, Commodore, various other formats as well)
- [FC5025](http://deviceside.com/fc5025.html) (5.25" only; Apple, Atari, Commodore, IBM PC)
- [FluxEngine](http://cowlark.com/fluxengine/index.html) (universal; many formats, including usual suspects + Brother, Acorn, etc.; hardware requires assembly)
- [Greaseweazle](https://github.com/keirf/Greaseweazle/wiki) (universal; many formats; hardware requires assembly)
- [Pauline](https://www.youtube.com/watch?v=PZiyms6lo4U&feature=emb_title) (universal; many formats; hardware/project in beta)
- [SuperCard Pro](https://www.cbmstuff.com/proddetail.php?prod=SCP) (3.5 or 5.25", adapter for 8"; many formats)
- [ZoomFloppy](https://store.go4retro.com/zoomfloppy/) (Commodore only)
---
INCLUDED WITH CAVEATS
- [Kryoflux](https://www.kryoflux.com/) (3.5 and 5.25"; various formats; product comes with weird anti-compete licensing clause, company more or less price-gouges for institutional use)
- [DiscFerret](www.discferret.com) (boards no longer available for purchase and hardware dev seems dead)
- [Catweasel](https://en.wikipedia.org/wiki/Individual_Computers_Catweasel) (boards no longer available for purchase and hardware dev seems dead; no software updates since 2009)

## Optical Media Ripping Hardware
- [Nimbie](https://disc.acronova.com/product/auto-blu-ray-duplicator-publisher-ripper-nimbie-usb-nb21/9/review.html)
- [RipStation](https://www.mfdigital.com/ripstation.html)

## Reading - Guides - Resources
- ["Media Matters: Media Matters: developing processes for preserving digital objects on physical carriers at the National Library of Australia"](https://www.nla.gov.au/content/media-matters-developing-processes-for-preserving-digital-objects-on-physical-carriers-at), Douglas Elford, Nicholas del Pozo, Snezana Mihajlovic, David Pearson, Gerard Clifton, Colin Webb, 2008-08-14
- ["Creating Virtual CD-ROM Collections"](http://www.ijdc.net/article/view/128/131), Kam Woods, Geoffrey Brown, *International Journal of Digital Curation*, Issue 4, no. 2, 2009-10-15
- ["Acquisition and Processing of Disk Images to Further Archival Goals"](https://ils.unc.edu/callee/archiving-2012-woods-lee.pdf), Kam Woods, Christopher A. Lee, *Proceedings of Archiving 2012*, 147–52, 2012
- ["Automating Disk Imaging"](https://openpreservation.org/blogs/automating-disk-imaging), Dirk von Suchodoletz, *Open Preservation Foundation blog*, 2012
- [Disk Image Formats](https://wiki.harvard.edu/confluence/display/digitalpreservation/Disk+Image+Formats), Format Matrix and Metadata Analysis reports by Bertram Lyons, *Harvard Library Digital Preservation Services wiki*, 2016
- ["Creating Disk Images of Born Digital Content: A Case Study Comparing Success Rates of Institutional Versus Private Collections"](http://dx.doi.org/10.1080/13614576.2016.1251849), Denise de Vries and Melanie Swalwell, *New Review of Information Networking*, Vol. 21, No. 2, pp. 129-140, 2016-11-26 ([PDF](/PDFs/Creating_Disk_Images_Case_Study.pdf))
- [Disk Imaging Resources, MoMA Media Conservation Initiative](https://www.mediaconservation.io/disk-imaging), 2017
- ["Towards Best Practices in Disk Imaging: A Cross-Institutional Approach"](http://resources.culturalheritage.org/emg-review/volume-6-2019-2020/colloton/), Eddy Colloton, Jonathan Farbowitz, Flamina Fortunato, Caroline Gil, *Electronic Media Review*, Vol. 6: 2019-2020.
- [OSSArcFlow](https://educopia.org/ossarcflow/), *Educopia Institute*, 2020
- ["Balancing Care and Authenticity in Digital Collections: A Radical Empathy Approach to Working with Disk Images"](https://journals.litwinbooks.com/index.php/jclis/article/view/125/102), Monique Lassere and Jess M. Whyte, *Journal of Critical Library and Information Studies*, Vol. 3, 2021-01-24
- [Disk Imaging Guide, Time-Based Media Conservation, Tate](https://www.tate.org.uk/file/disk-imaging-guide-pdf), Tom Ensom, updated 2021-01
- [BitCurator Environment Confluence wiki](https://confluence.educopia.org/display/BC/BitCurator+Environment)
- [Digital Archiving traNsfer / iNgest / packagiNg Group (DANNNG)](https://dannng.github.io/)

### Floppies
- ["Rescuing Floppy Disks"](https://www.archiveteam.org/index.php?title=Rescuing_Floppy_Disks), Archive Team
- [The Archivist's Guide to Kryoflux](https://github.com/archivistsguidetokryoflux/archivists-guide-to-kryoflux)
- ["Project Kryoflux" blog series](https://goughlui.com/2013/04/21/project-kryoflux-part-1-the-board-and-associated-hardware/), *Dr. Gough's Tech Zone*
- [Floppy Disk Preservation](https://diskpreservation.com/)
- ["A Dogged Pursuit: Capturing Forensic Images of 3.5" Floppy Disks"](https://practicaltechnologyforarchives.org/issue2_waugh/), Dorothy Waugh, *Practical Technology for Archives*, 2014-05-29
- ["Digital Archaeology and/or Forensics: Working with Floppy Disks from the 1980s"](https://journal.code4lib.org/articles/11986), John Durno, *The Code4Lib Journal*, Issue 34, 2016-10-25
- ["IBM PC Floppy Disks - A Deeper Look at Disk Formats and Copy Protection"](https://nerdlypleasures.blogspot.com/2015/11/ibm-pc-floppy-disks-deeper-look-at-disk.html)
- ["A Pirate's Life for Me, Part 3: Case Studies in Copy Protection"](https://www.filfre.net/2016/01/a-pirates-life-for-me-part-3-case-studies-in-copy-protection/), Jimmy Maher, *The Digital Antiquarian*
- ["Confessions of a Disk Cracker: The Secrets of 4am"](https://paleotronic.com/2018/06/15/confessions-of-a-disk-cracker-the-secrets-of-4am/), *Paleotronic*
- ["Atari Floppy Disk Copy Protection"](http://dmweb.free.fr/files/Atari-Copy-Protection-V1.4.pdf), Jean Louis-Guerin (DrCoolZic), 2015-06-24
- ["The cleverest floppy disc protection ever? Western Security Ltd"](https://scarybeastsecurity.blogspot.com/2020/12/the-cleverest-floppy-disc-protection.html), *Security: Hacking everything*, Chris Evans, 2020-12-14
- ["8" Disk Recovery: Kryoflux and Catweasel"](https://openpreservation.org/blogs/8-disk-recovery-kryoflux-and-catweasel/), Denise de Vries, *Open Preservation Foundation blog*, 2016-09-14

### Optical
- ["Preserving optical media from the command line"](http://blog.kbresearch.nl/2015/11/13/preserving-optical-media-from-the-command-line/), Johan van der Knijf
- ["An Optical Media Preservation Strategy for New York University’s Fales Library & Special Collections"](https://archive.nyu.edu/bitstream/2451/43877/2/Schweikert_OpticalMediaPreservationNYU_2018.pdf), Annie Schweikert
- ["An Introduction to Optical Media Preservation"](https://www.weareavp.com/wp-content/uploads/2017/07/OpticalMediaPreservation.pdf), Alex Duryee
- ["Imaging CD-Extra / Blue Book discs"](https://openpreservation.org/blogs/imaging-cd-extra-blue-book-discs/), Johan van der Knijf
- ["To Image or Copy - The Compact Disc Digital Audio Dilemma"](https://campuspress.yale.edu/borndigital/2016/12/20/to-image-or-copy-the-compact-disc-digital-audio-dilemma/), Alice Prael
- ["Developing Virtual CD-ROM Collections: The Voyager Company Publications"](https://doi.org/10.2218/ijdc.v7i2.226), Geoffrey Brown, *International Journal of Data Curation*, Vol. 7, No. 2, 2012-10-23 ([Wayback](https://web.archive.org/web/20190911005149/http://www.ijdc.net:80/article/view/216/285))
- [Workflow, Flashback project](https://coptr.digipres.org/Workflow:Flashback_project_-_optical_media_imaging_workflow), British Library via COPTR
- ["Assessing High-Volume Transfers from Optical Media at NYPL"](https://journal.code4lib.org/articles/15908), Michelle Rothrock, Alison Rhonemus, Nick Krabbenhoeft, *The Code4Lib Journal*, Issue 51, 2021-06-14
- ["To Everything There Is a Session: A Time to Listen, a Time to Read Multi-session CDs"](https://journal.code4lib.org/articles/17208), Dianne Dietrich, Alex Nelson, *The Code4Lib Journal*, Issue 56, 2023-04-21
- ["The Forgotten Disc: Synthesis and Recommendations for Viable VCD Preservation"](https://journal.code4lib.org/articles/17406), Andrew Weaver, Ashley Blewer, *The Code4Lib Journal*, Issue 57, 2023-08-29

### Hard Drives
- ["How to Do Everything with dd"](https://www.linuxquestions.org/linux/answers/Applications_GUI_Multimedia/How_To_Do_Eveything_With_DD)

### Data Tape
- ["Recovery of heritage software stored on magnetic tape for Commodore microcomputers"](https://doi.org/10.2218/ijdc.v11i2.386), Denise de Vries, Craig Harrington, *International Journal of Data Curation*, Vol. 11, No. 2, 2016
- ["Recovering '90s Data Tape - Experiences from the KB Archaeology project (iPres 2019 paper)"](https://www.bitsgalore.org/2019/09/09/recovering-90s-data-tapes-experiences-kb-web-archaeology), Johan van der Knijf, *bitsgalore*, 2019-09-09
